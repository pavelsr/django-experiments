from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'demka.views.home', name='home'),
    # url(r'^demka/', include('demka.foo.urls')),

	url(r'^$',  TemplateView.as_view(template_name = 'index.html'), name = 'index'),
	url(r'^test1/', include('test1.urls')),
	url(r'^test2/', include('test2.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^admin/', include(admin.site.urls)),
)
